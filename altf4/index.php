<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Altf4 script tags</title>
</head>

<body>

    <div id="display-compliance">

    </div>

    <script>
        (function(w, d, s, o, f, js, fjs) {
            w['365Compliance'] = o;
            w[o] = w[o] || function() {
                (w[o].q = w[o].q || []).push(arguments)
            };
            js = d.createElement(s), fjs = d.getElementsByTagName(s)[0];
            js.id = o;
            js.src = f;
            js.async = 1;
            fjs.parentNode.insertBefore(js, fjs);
        }(window, document, 'script', 'mw', 'https://app.popia365.co.za/js/365Compliance.min.js'));
        mw('init', {
            container: 'display-compliance',
            key: '66a45bdb-9d22-45d9-8403-1d49ffb872b6'
        });
        mw('pn');
    </script>
</body>

</html>